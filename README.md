# dotfiles

```
git init --bare $HOME/.dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME' (add this alias to .bashrc)
bash
config config --local status.showUntrackedFiles no
```

https://www.atlassian.com/git/tutorials/dotfiles  
https://www.youtube.com/watch?v=tBoLDpTWVOM

## fonts

- [ttf-fira-code](https://archlinux.org/packages/community/any/ttf-fira-code/)
- [ttf-nerd-fonts-symbols](https://archlinux.org/packages/community/any/ttf-nerd-fonts-symbols/)

## shells

- zsh
- dash
- bash

## editors

- [nvim](https://wiki.archlinux.org/index.php/Neovim)
- VScodium

## packages

- [sway](https://archlinux.org/packages/community/x86_64/sway/)
- [swaylock](https://archlinux.org/packages/community/x86_64/swaylock/)
- [dunst](https://wiki.archlinux.org/index.php/Dunst)
- [mpv](https://wiki.archlinux.org/index.php/Mpv)
- [fzf](https://archlinux.org/packages/community/x86_64/fzf/)
- [ffmpeg](https://archlinux.org/packages/extra/x86_64/ffmpeg/)
- [wl-clipboard](https://archlinux.org/packages/community/x86_64/wl-clipboard/)
- [zathura](https://wiki.archlinux.org/index.php/Zathura)
- zathura-pdf-mupdf
- [firefox](https://wiki.archlinux.org/index.php/Firefox)
- [newsboat](https://newsboat.org/)
- [doas](https://archlinux.org/packages/community/x86_64/opendoas/)
- bemenu
- lf
- foot
- git
- paru
