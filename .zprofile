#!/bin/zsh

# PATH
export PATH=$PATH:/usr/lib/rstudio

# Default programs
export EDITOR="nvim"
export TERMINAL="foot"
export BROWSER="firefox"

# XDG
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CURRENT_DESKTOP="sway"

export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"

# SHELL
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/shell/inputrc"

# WAYLAND
export MOZ_ENABLE_WAYLAND=1 firefox
export MOZ_DBUS_REMOTE=1
export QT_QPA_PLATFORM=wayland

# JUPYTER
export JUPYTER_CONFIG_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/jupyter"
export JUPYTER_PLATFORM_DIRS="1"

# PYTHON
export PYTHON_HISTORY="$XDG_STATE_HOME/python/history"
export PYTHONPYCACHEPREFIX="${XDG_CACHE_HOME:-$HOME/.cache}/python"
export PYTHONUSERBASE="$XDG_DATA_HOME/python"

export LESSHISTFILE=/dev/null
export NODE_REPL_HISTORY="$XDG_STATE_HOME/node_repl_history"
export R_ENVIRON="${XDG_CONFIG_HOME:-$HOME/.config}/R/Renviron"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export DOCKER_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/docker"

# BEMENU
export BEMENU_OPTS="--scrollbar 'autohide' --wrap --list 10 --fn 'Fira Code 10.5' --tb '#1d1d1d' --fb '#1d1d1d' --nb '#1d1d1ddd' --ab '#1d1d1ddd' --hb '#000000dd' --tf '#ff69b4' --ff '#35bf5c' --nf '#3399ff' --af '#3399ff' --hf '#ff9f00' --scb '#000000dd' --scf '#ff69b4'"
