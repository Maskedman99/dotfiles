#!/bin/dash

case "$(file -Lb --mime-type -- "$1")" in
    image/*)
        chafa -f sixel -s "$2x$3" --animate off --polite on "$1"
        exit 1
        ;;
    application/pdf)
        pdftoppm -jpeg -f 1 -singlefile "$1" | chafa -f sixel -s "$2x$3" --animate off --polite on
        exit 1
        ;;
    audio/*)
        ffprobe -i "$1" -of flat -v quiet -unit -sexagesimal -show_entries stream_tags:format_tags:format=duration,size:stream=codec_type,codec_long_name,bit_rate,sample_rate -prefix | awk -F'["=]' ' 
        {
          gsub(/^[[:space:]]+|[[:space:]]+$/, "");  # Remove leading/trailing spaces
          key = $1;
          value = $3;
          last_word = gensub(/.*\./, "", "1", key);  # Extract the last word after the last dot
          formatted_key = toupper(substr(last_word, 1, 1)) gensub(/_/, " ", "g", substr(last_word, 2));
          printf "%s: %s\n", formatted_key, value;
        }
        '
        ;;
    application/zip)
        bsdtar -tf "$1"
        ;;
    *)
        cat "$1"
        ;;
esac

