#!/bin/dash

DEFAULT_COMMAND="firefox";
LIST_OF_COMMANDS="firefox\nwl-copy\nchromium";

FOCUSED=$(swaymsg -t get_tree | grep -A 50 '"focused": true' | grep name | awk -F': "'  '{print $2}' | awk -F'",' '{print $1}')

if echo "$FOCUSED" | grep -q "\- Chromium"; then
  COMMAND="chromium"
else
  COMMAND="$DEFAULT_COMMAND"
fi

BOOKMARK=$(cat $XDG_CONFIG_HOME/bookmarks | bemenu -p "$COMMAND")

while [ "$BOOKMARK" = "---------- CHANGE COMMAND -----------" ]; do 
  COMMAND=$(echo "$LIST_OF_COMMANDS" | bemenu -p "Select a command")
  [ -z "$COMMAND" ] && COMMAND="$DEFAULT_COMMAND"
  BOOKMARK=$(cat $XDG_CONFIG_HOME/bookmarks | bemenu -p "$COMMAND")
done

[ -z "$BOOKMARK" ] && exit 0;

$COMMAND "$BOOKMARK" > /dev/null
