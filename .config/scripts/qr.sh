#!/bin/dash

# Use a menu command to accept the input
input=$(exec wl-paste | bemenu -p "Enter text:");

# Exit if input is empty
[ -z "$input" ] && exit 0;

# Directory used to store the files temporarily
cachedir="/tmp/qr";
[ ! -d "$cachedir" ] && mkdir -p "$cachedir";

# Encode the input to QR and store the image
qrencode "$input" -o "$cachedir/qr.png";
# Display the image
mpv --really-quiet "$cachedir/qr.png";

# Once finished, remove all the cached files
rm "${cachedir:?}"/*;
