#!/bin/dash

CURRENTWORKSPACES=$(swaymsg -t get_tree | grep current_workspace | cut -d'"' -f 4)
OUTPUTS=$(swaymsg -pt get_outputs | grep Output | cut -d' ' -f 2)
FOCUSEDOUTPUT=$(swaymsg -pt get_outputs | grep "(focused)" | cut -d' ' -f 2)

for output in $OUTPUTS; do
  swaymsg focus output "$output"
  swaymsg workspace LOCK-"$output"
  swaymsg "exec $TERMINAL -e dash -c 'sleep 0.1 && cmatrix -b'"
done

swaymsg bar mode invisible
swaylock && killall cmatrix

for currentworkspace in $CURRENTWORKSPACES; do
  swaymsg "workspace "$currentworkspace""
done

swaymsg bar mode toggle
swaymsg focus output "$FOCUSEDOUTPUT"

dunstify "Welcome Back" -a Sway -t 2000
