#!/bin/dash

BOOKMARK="$(wl-paste --primary)"
FILE="$XDG_CONFIG_HOME/bookmarks"

if grep -q "^$BOOKMARK$" "$FILE"; then
  dunstify "$BOOKMARK already exists." -a Oops -t 5000
else
  echo "$BOOKMARK" >> "$XDG_CONFIG_HOME/bookmarks" && dunstify "$BOOKMARK saved." -a Success -t 5000
fi
