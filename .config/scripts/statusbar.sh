#!/bin/bash

# The Sway configuration file in ~/.config/sway/config calls this script.
# You should see changes to the status bar after saving this script.
# If not, do "killall swaybar" and $mod+Shift+c to reload the configuration.

green="#35bf5c" 
red="#ff0000"
blue="#3399ff"
orange="#ff9f00"
black="#000000"
white="#eaeaea"
pink="#ff69b4"

wifiConnectedIcon=" "
wifiDisconnectedIcon=" 󰖪 "
ethernetConnectedIcon="󰈀"
batteryChargingIcon="󰂈"
batteryIcon="󰁽"
pluggedInIcon=""

kernel_version=" $(uname -r | cut -d '-' -f1)"

common() {
  printf "\"separator\":false,\"separator_block_width\":4,"
  printf "\"border\": \"$white\",\"border_left\":1,\"border_right\":0,\"border_bottom\":0,\"border_top\":0,},"
}

kernel() {
  printf "{\"name\":\"id_kernel\",\"color\":\"$orange\",\"full_text\":\"$kernel_version\"," 
  common
}

datetime(){
  printf "{\"name\":\"id_time\",\"color\":\"$white\",\"full_text\":\"$(date '+ %a %d-%h-%Y %l:%M %p')\","
  common
}

battery(){
  # Returns the battery status: "Full", "Discharging", or "Charging"
  battery_status=$(cat /sys/class/power_supply/BAT0/status)
  if [ "$battery_status" = "Full" ] ; then battery_icon="$pluggedInIcon" ; 
  elif [ "$battery_status" = "Charging" ] ; then battery_icon="$batteryChargingIcon" ; 
  else battery_icon="$batteryIcon" ; fi

  charge=$(cat /sys/class/power_supply/BAT0/capacity)
  if [ $charge -eq 100 ] ; then color=$green ; elif [ $charge -gt 50 ] ; then color=$orange ; else color=$red ; fi
  
  printf "{\"name\":\"id_battery\",\"color\":\"$color\",\"full_text\":\"$battery_icon $charge\","
  common
}

diskfree(){ 
  printf "{\"name\":\"id_disk\",\"color\":\"$pink\",\"full_text\":\" $(df -H -l --output='avail' "$HOME" | grep -E -o '[0-9]+G')\","
  common
}

memory(){
  printf "{\"name\":\"id_memory\",\"color\":\"$green\","
  printf "\"full_text\":\"󰍛 $(free -h --si | grep Mem | awk '{ printf("%s - %s", $3,$4) }')\","
  common
}

load(){ 
  printf "{\"name\":\"id_cpu\",\"color\":\"$blue\","
  printf "\"full_text\":\" $(cat /proc/loadavg | awk -F ' ' '{print $2}')\","
  common
}

brightness(){ 
  printf "{\"name\":\"id_brightness\",\"color\":\"$orange\","
  printf "\"full_text\":\"󰃠 $(brightnessctl | grep Current | awk -F'[(%)]' '{print $2}')\","
  common
}

network(){ 
  wireless=$(nmcli -t -f NAME,TYPE connection show -active | grep -v lo:loopback | grep :\*-wireless$ | cut -d':' -f1)
  ethernet=$(nmcli -t -f NAME,TYPE connection show -active | grep -v lo:loopback | grep :\*-ethernet$ | cut -d':' -f1)
  [ -n "$ethernet" ] && ethernet="$ethernetConnectedIcon $ethernet"
  [ -n "$wireless" ] && wireless="$wifiConnectedIcon $wireless"

  local networkStatus=$(nmcli -t -f STATE general)
  if [ "$networkStatus" == "disconnected" ]; then
    networkColor="$red"
    networkText="$wifiDisconnectedIcon"
  elif [ "$networkStatus" == "connected (site only)" ]; then
    networkColor="$red" 
    networkText="$wireless $ethernet"
  elif [ "$networkStatus" == "connecting" ]; then
    networkColor="$orange"
    networkText="$wireless $ethernet"
  else
    networkColor="$green"
    networkText="$wireless $ethernet"
  fi
  
  printf "{\"name\":\"id_network\","
  printf "\"color\":\"$networkColor\",\"full_text\":\"$networkText\","
  common
}

volume(){  
  # ALSA only with amixer
  #local text=$(amixer get Master | awk '/Mono.+/ {print $6=="[off]"?" MUTE":" "$4}' | tr -d '[%]')
  
  # Pipewire, Wireplumber, ALSA, pipwire-alsa
  local volumeText=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{split($0,a," "); print a[3]=="[MUTED]"?" MUTE":" "a[2]*100}')
  [ "$volumeText" != ' MUTE' ] && volumeColor="$pink" || volumeColor="$red"
  printf "{\"name\":\"id_volume\",\"color\":\"$volumeColor\",\"full_text\":\"%s\"," "$volumeText"
  common
}

media(){
  local media_status=$(playerctl status)

  if [[ -z "$media_status" ]]; then
    return
  fi
  [ "$media_status" = 'Playing' ] && printf "{\"name\":\"id_media\",\"color\":\"$green\"," || printf "{\"name\":\"id_media\",\"color\":\"$blue\","
  printf "\"full_text\":\" $(playerctl metadata -f '{{ artist }} - {{ title }}' | sed 's/"//g' | sed 's/%/%%/g')\","
  common
}

echo '{ "version": 1, "click_events":true }'
echo '['
echo '[]'

(while :
do 
  printf ",["
  media
  kernel
  diskfree
  memory
  load
  battery
  network
  brightness
  volume
  datetime
  printf "]"
# Refrsh Rate
  sleep 10
done) &

# Listening for STDIN events
while read line;
do
  # echo $line > /tmp/tmp.txt
  # on click, we get from STDIN :
  # {"name":"id_time","button":1,"modifiers":["Mod2"],"x":2982,"y":9,"relative_x":67,"relative_y":9,"width":95,"height":22}

  # DATE click
  if [[ $line == *"name"*"id_time"* ]]; then $TERMINAL --title=launcher -e dash -c 'cal -my; sleep infinity' &
  # CPU click
  elif [[ $line == *"name"*"id_cpu"* ]]; then $TERMINAL --title=launcher -e dash -c 'top -E g;' &
  # Network click
  elif [[ $line == *"name"*"id_network"* ]]; then $TERMINAL --title=launcher -e dash -c 'nmtui;' &
  # Volume click
  elif [[ $line == *"name"*"id_volume"* ]]; then $TERMINAL --title=launcher -e dash -c 'alsamixer;' &
  # Kernel click
  elif [[ $line == *"name"*"id_kernel"* ]]; then $TERMINAL --title=launcher -e dash -c 'fastfetch; sleep infinity' &
  # Disk click
  elif [[ $line == *"name"*"id_disk"* ]]; then $TERMINAL --title=launcher -e dash -c '
    lsblk -o NAME,SIZE,RM,TYPE,FSTYPE,FSUSE%,MOUNTPOINTS,STATE | sed -e '1s/^/'$(tput bold)$(tput setaf 2)'/' \
    -e '1s/$/'$(tput sgr0)'/' \
    -e '/running/s/^/'$(tput setaf 4)'/' \
    -e '/running/s/$/'$(tput sgr0)'/' \
    -e '/SWAP/s/^/'$(tput setaf 5)'/' \
    -e '/SWAP/s/$/'$(tput sgr0)'/' \
    -e '/\//s/^/'$(tput setaf 2)'/' \
    -e '/\//s/$/'$(tput sgr0)'/';
    sleep infinity;' &
  fi

done
