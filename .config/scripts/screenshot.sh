#!/bin/dash

LIST_OF_MODES="Region\nWindow\nFull-Screen";
LIST_OF_OUTPUT_MODES="Save to file\nwl-copy";
SAVE_FORMAT="screenshot_%Y_%m_%d_%H_%M_%S.png"

NOTIFY_COPY_SUCCESS="dunstify Screenshot copied -a Success -t 5000";
NOTIFY_SAVE_SUCCESS="dunstify Screenshot saved -a Success -t 5000";

MODE=$(echo "$LIST_OF_MODES" | bemenu -p "Screenshot")
[ -z "$MODE" ] && exit 0;
OUTPUT_MODE=$(echo "$LIST_OF_OUTPUT_MODES" | bemenu -p "Screenshot")
[ -z "$OUTPUT_MODE" ] && exit 0;

[ "$MODE" = "Region" ] && [ "$OUTPUT_MODE" = "wl-copy" ] && grim -g "$(slurp)" - | wl-copy && $NOTIFY_COPY_SUCCESS
[ "$MODE" = "Region" ] && [ "$OUTPUT_MODE" = "Save to file" ] && grim -g "$(slurp)" $HOME/Pictures/$(date +"$SAVE_FORMAT") && $NOTIFY_SAVE_SUCCESS

[ "$MODE" = "Full-Screen" ] && [ "$OUTPUT_MODE" = "wl-copy" ] && grim - | wl-copy && $NOTIFY_COPY_SUCCESS
[ "$MODE" = "Full-Screen" ] && [ "$OUTPUT_MODE" = "Save to file" ] && grim $HOME/Pictures/$(date +"$SAVE_FORMAT") && $NOTIFY_SAVE_SUCCESS
