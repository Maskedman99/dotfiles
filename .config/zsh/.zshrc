# Enable Colors
autoload -U colors && colors
# Prompt
PS1="%B%{$fg[green]%}%c %{$fg[blue]%}$%{$reset_color%}%b "

# Interactive 
setopt interactive_comments

# History in cache directory
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=$XDG_STATE_HOME/zsh_history

# Load aliases, shortcuts and functions if existent
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/functionsrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/functionsrc"

# Auto/tab completion
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit -d $XDG_CACHE_HOME/zsh/zcompdump
# Include hidden files
_comp_options+=(globdots)

# Bindings
bindkey -s '^o' 'lfcd\n'

# Load autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
# Load syntax highlighting; should be last.
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null
