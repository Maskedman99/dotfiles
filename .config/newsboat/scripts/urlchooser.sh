#!/bin/sh

EXT=$(echo $1 | awk -F'/' '{print $NF}' | awk -F. '{print $NF}')
DOM=$(echo $1 | awk -F/ '{print $3}')

case $EXT in
	"jpg"* | "png"* | "jpeg"* | "JPG"* | "webp" | "gif" | "mp4") mpv -quiet $1 >/dev/null 2>&1 &;;
	*) firefox $1 >/dev/null 2>&1 &;;
esac


